# 从一个已有的文件目录初始化一个仓库

如，已经存在一个目录和文件，需要将这个目录初始化成一个`learning-git`的仓库。

![image-20231020184324674](http://qn.zxiuwu.online/typora/20231020184325.png)

## 初始化本地仓库

执行`git init -b "main"` ，会在本地创建一个文件夹`.git` ，同时创建一个本地分支"main"

![image-20231020192512733](http://qn.zxiuwu.online/typora/20231020192513.png)

## 本地仓库关联远程仓库

如，将本地仓库，关联到gitee上的仓库`https://gitee.com/zxiuwu/learning-git.git`中，

执行`git remote add origin <remote_url>`

![image-20231020192054482](http://qn.zxiuwu.online/typora/20231020192054.png)

**如果远程仓库还没有分支，则先上传`main`分支到远程仓库** 

`git branch --set-upstream-to=origin/<branch> main`

查看当前仓库状态`git status`

![image-20231020193246832](http://qn.zxiuwu.online/typora/20231020193247.png)

添加文件，并提交到本地仓库



拉取远程分支 `git pull <remote> <branch>`

